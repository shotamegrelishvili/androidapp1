import React, { useRef, useState, useEffect } from 'react';
import { Animated, Easing } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions
} from "react-native/Libraries/NewAppScreen";

import Header from './Header';
import Content from './Content';

const FadeOutView = (props) => {
  const fadeAnim = useRef(new Animated.Value(1)).current  // Initial value for opacity: 1
  const [isStartPageShown, setIsStartPageShown] = useState(true);

  useEffect(() => {
    setTimeout(()=>{
      setIsStartPageShown(false);
    }, 1000);

    Animated.timing(
      fadeAnim,
      {
        toValue: 0,
        duration: 1000,
        easing: Easing.exp,
        useNativeDriver: true
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 // Special animatable View
      style={{
      ...props.style,
          opacity: fadeAnim,         // Bind opacity to animated value
          display: isStartPageShown ? 'flex': 'none',
      }}
    >
      {props.children}
    </Animated.View>
  );
};

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current  // Initial value for opacity: 1
  const [isPassportPageShown, setIsPassportPageShown] = useState(false);

  useEffect(() => {
    setTimeout(()=>{
      setIsPassportPageShown(true);
    }, 3000);

    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 3000,
        // easing: Easing.exp,
        useNativeDriver: true
      }
    ).start();
  }, [fadeAnim])

  return (
    <Animated.View                 // Special animatable View
      style={{
      ...props.style,
          opacity: fadeAnim,         // Bind opacity to animated value
          display: isPassportPageShown ? 'flex': 'none',
      }}
    >
      {props.children}
    </Animated.View>
  );
};

const App: () => Node = () => {
// <LinearGradient colors={['#34b175', '#216847']} style={styles.linearGradient}>
  const [isContentReloading, setContentReloading] = useState(false);

  const callReload = () =>
  {
    setContentReloading(true);
    setTimeout(() => {
      setContentReloading(false);
    }, 3000);
  };

  return (
    <SafeAreaView style={{
      flex: 1,
    }}>
      <StatusBar barStyle={'light-content'} />
      <FadeOutView style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
      }}>
        <Image source={require('./images/cross_big_greenbg.png')} style={styles.mainPageCross}/>
      </FadeOutView>
      <FadeInView style={{
        flex: 1
      }}>
        <Header style={{
          flex: 1
        }} callReload={callReload}>
          <Text>?</Text>
          <View>
            <Image source={require('./images/cross_big_greenbg.png')} style={styles.PassportPageCross}/>
          </View>
        </Header>
        <View style={{
          flex: 6,
          overflow: 'visible'
  }}>
          <Content isReloading={isContentReloading}/>
        </View>
      </FadeInView>
    </SafeAreaView>
    );
}

const styles = StyleSheet.create({
  mainPageCrossContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // margin: '10 auto',
    // textAlign: 'center',
    // backgroundColor: '#ff0000'
  },
  mainPageCross: {
    width: 100,
    height: 100,
  },
  passportPageCross: {
    width: 60,
    height: 60,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: '0px',
    background: 'rgb(52,168,113)',
    padding: 8,
  },
  plus: {
    // display: 'block',
    fontSize: 144,
    lineHeight: 124,
    color: '#fff',
    fontFamily: '',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paragraph: {
    margin: 0,
    fontSize: 18,
    color: '#fff',
    fontFamily: 'monospace',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
