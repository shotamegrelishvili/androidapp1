## To run emulation on Windows:
npm i --save-dev @react-native-community/cli
npx react-native-windows-init --overwrite
npx react-native run-windows

## Prerequisites
1. Install JAVA SDK 15.0.2
2. Install Android SDK
3. Edit and rename the file `./android/local.properties.example` to `local.properties`
4. Create Private Key as described: `https://reactnative.dev/docs/signed-apk-android` and store it in `./app/app.keystore`
5. Edit and rename the file `./android/gradle.properties.example` to `gradle.properties` according to you key file

## Build APK for Android
### (Android phone should be connected via USB and visible through adb devices)
npx react-native run-android --variant=release
