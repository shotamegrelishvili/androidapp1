/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow strict-local
 * @format
 */

import type {Node} from 'react';
import {ImageBackground, StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
// import Colors from './Colors';
// import HermesBadge from './HermesBadge';



const Header = ({callReload}): Node => {
// Binding function with this class.

  return (
    <View style={{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#f7f5f6',
      borderBottomColor: '#cfcdd0',
      borderBottomWidth: 1,
      shadowColor: '#ff0000',shadowOffset: { width: 10, height: 10 },
      shadowColor: 'black',
      shadowOpacity: 0.2,
      elevation: 3,
      shadowRadius: 3,
  }}>
    <View style={{
      flex: 1,
      alignItems: 'center',
        // margin: 24
  }}>
    <TouchableOpacity activeOpacity={.5} onPress={callReload}>
      <Image source={require('./images/reload_icon.png')} style={styles.reloadIcon}/>
    </TouchableOpacity>
    </View>
    <View style={{
      flex: 6,
      alignItems: 'center',
  }}>
    <Image source={require('./images/cross_big_greenbg.png')} style={styles.crossIcon}/>
    </View>
    <View style={{
      flex: 1
  }}>
    <Image source={require('./images/hamburger_menu_icon.png')} style={styles.menuIcon}/>
    </View>
  </View>
);
};

const styles = StyleSheet.create({
  reloadIcon: {
    width: 24,
    height: 24,
  },
  menuIcon: {
    width: 32,
    height: 32,
  },
  crossIcon: {
    width: 64,
    height: 64
  },
  background: {
    paddingBottom: 40,
    paddingTop: 96,
    paddingHorizontal: 32,
  },
  logo: {
    opacity: 0.2,
    overflow: 'visible',
    resizeMode: 'cover',
    /*
     * These negative margins allow the image to be offset similarly across screen sizes and component sizes.
     *
     * The source logo.png image is 512x512px, so as such, these margins attempt to be relative to the
     * source image's size.
     */
    marginLeft: -128,
    marginBottom: -192,
  },
  text: {
    fontSize: 40,
    fontWeight: '700',
    textAlign: 'center',
  },
});

export default Header;
