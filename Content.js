/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow strict-local
 * @format
 */

import type {Node} from 'react';
import {StyleSheet, View, Text, Image, ScrollView, ActivityIndicator } from 'react-native';
import React from 'react';

const Content = ({isReloading}): Node => {
  const today  = new Date();
  let currentHours = today.getHours();
  currentHours = ("0" + currentHours).slice(-2);
  let currentMinutes = today.getMinutes();
  currentMinutes = ("0" + currentMinutes).slice(-2);
  const datetime = today.getDate() + '.' + +(today.getMonth()+1) + '.' + today.getFullYear() +' '
    + currentHours + ':' + currentMinutes;

  return (
  <View>
    <View style={{
      flex: 1,
      margin: 50,
      alignItems: 'center',
      display: !isReloading ? 'none' : 'flex',
    }}>
      <ActivityIndicator size="large" color="#1c583c" />
    </View>
    <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      style={{
      display: isReloading ? 'none' : 'flex',
      backgroundColor: '#f7f5f6',
  }}>
      <View style={{
        flex: 1,
        margin: 10,
        marginTop: 20,
        backgroundColor: '#1c583c',
        alignItems: 'center',
        padding: 16,
        borderRadius: 6
  }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
      }}>
            <Text style={{
              color: '#fff',
              flex: 6,
              textAlign: 'center',
              fontSize: 16,
              fontFamily: 'bpg_extrasquare_2009',
            }}>
              {
                'Michael Katz'
              }
            </Text>
            <Image source={require('./images/dropdown_1.png')} style={styles.dropdownIcon}/>
        </View>
      </View>
      <View style={{
        margin: 10,
        flex: 1,
        color: '#1c583c',
        justifyContent: 'center',
        alignItems: 'center',
    }}>
        <Text style={{
          // fontFamily: 'bpg_extrasquare_2009',
          fontSize: 18,
          // fontWeight: 'bold',
            color: '#1c583c',
          fontFamily: 'bpg_extrasquare_mtavruli_2009'
        }}>
          პ/ნ: {
    '33919620'
  }
        </Text>
      </View>
      <View style={{
        flex: 1,
        margin: 10,
        flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center',
      }}>
        <View style={{
          flex: 1,
          alignItems: 'flex-end',
          paddingRight: 10,
        }}>
          <Image source={require('./images/StatusOK.png')} style={styles.StatusOKIcon}/>
        </View>
        <View style={{
          flex: 3,
        }}>
        <Text style={{
            marginTop: 10,
            fontSize: 24,
            // fontWeight: 'bold',
            color: '#1c583c',
            fontFamily: 'bpg_extrasquare_mtavruli_2009'
  }}>
          სტატუსი: მწვანე
          </Text>
        </View>
      </View>
      <View style={{
          flex: 1,
            flexDirection: 'row',
          // justifyContent: 'center',
          // alignItems: 'center',
        }}>
        <View style={{
            flex: 1,
              alignItems: 'flex-end',
              paddingRight: 10,
          }}>
          <Text style={{
            fontSize: 15,
            fontFamily: 'bpg_extrasquare_mtavruli_2009',
            color: '#1c583c',
          }}>
            განახლებულია:
              </Text>
          </View>
          <View style={{
            flex: 1,
          }}>
          <Text style={{
            fontSize: 15,
            fontFamily: 'bpg_extrasquare_mtavruli_2009',
            color: '#1c583c',
          }}>
            {datetime}
          </Text>
        </View>
      </View>
      <View style={[styles.Card, {
          padding: 6,
      }]}>
      <View style={{
          flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        }}>
          <Image source={require('./images/certificate_with_text.png')} style={styles.dropdownIcon}/>
        </View>
      </View>

      <View style={[styles.Card, {
        padding: 16,
        paddingTop: 32,
      }]}>
        <View style={{
              // justifyContent: 'center',
              // alignItems: 'center',
          }}>
            <Image source={require('./images/vaccinated_qr_code_katz.png')}
            style={{
              // display: 'block'
            }}/>
        </View>
      </View>

    <View style={styles.Card}>
      <Text style={{
        textAlign: 'center',
        fontSize: 17,
        fontFamily: 'bpg_extrasquare_2009',
        color: '#1c583c',
        paddingBottom: 10
  }}>COVID-19 ვაქცინაცია</Text>
    <View style={{flexDirection: 'row'}}>
        <View style={{
          flex: 1
        }}>
          <Text style={[styles.cardText, {
            fontWeight: '700'
          }]}>ვაქცინა</Text>
        </View>
        <View style={{
            flex: 1,
            alignItems: 'flex-end'
          }}>
        <Text style={[styles.cardText, {fontWeight: 'bold'}]}>თარიღი</Text>
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={{
            flex: 2
          }}>
        <Text style={styles.cardText}>SARS-CoV-2 ვაქცინა{"\n"}
          (Vero Cell) Sinopharm{"\n"}
          (კოვიდ 19-ის საწინააღმდეგო ვაქცინა) (კოვ/COV)
        </Text>
      </View>
      <View style={{
          flex: 1,
          alignItems: 'flex-end'
        }}>
        <Text style={styles.cardText}>20.09.2021</Text>
      </View>
    </View>
    <View style={{flexDirection: 'row', marginTop: 10}}>
      <View style={{
          flex: 2
        }}>
      <Text style={styles.cardText}>SARS-CoV-2 ვაქცინა{"\n"}
        (Vero Cell) Sinopharm{"\n"}
        (კოვიდ 19-ის საწინააღმდეგო ვაქცინა) (კოვ/COV)
        </Text>
        </View>
        <View style={{
          flex: 1,
            alignItems: 'flex-end'
        }}>
      <Text style={styles.cardText}>28.08.2021</Text>
        </View>
    </View>
    </View>

    <View style={styles.Card}>
      <Text style={[styles.cardText, {
      fontFamily: 'bpg_extrasquare_2009',
      textAlign: 'center',
      fontSize: 17,
      // fontWeight: 'bold',
      paddingBottom: 10
    }]}>ბოლო PCR კვლევა</Text>
      <Text style={styles.cardText}>არ ჩაუტარებია</Text>
    </View>

    <View style={styles.Card}>
      <Text style={[styles.cardText, {
        textAlign: 'center',
        fontSize: 17,
        // fontWeight: 'bold',
        paddingBottom: 10
      }]}>ბოლო სწრაფი ტესტი</Text>
      <View style={{flexDirection: 'row'}}>
        <View style={{
            flex: 2
          }}>
          <Text style={[styles.cardText, {fontWeight: 'bold'}]}>კვლევის თარიღი</Text>
        </View>
        <View style={{
          flex: 1,
            alignItems: 'flex-end'
        }}>
          <Text style={[styles.cardText, {fontWeight: 'bold'}]}>შედეგი</Text>
        </View>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={{
            flex: 2
          }}>
          <Text style={styles.cardText}>27.08.21</Text>
        </View>
        <View style={{
          flex: 1,
            alignItems: 'flex-end'
        }}>
          <Text style={styles.cardText}>Neg.
            <Image source={require('./images/print_icon.png')} style={styles.dropdownIcon}/>
        </Text>
        </View>
      </View>
    </View>

    <View style={styles.Card}>
      <Text style={[styles.cardText, {
      textAlign: 'center',
        fontSize: 17,
        // fontWeight: 'bold',
        paddingBottom: 10
    }]}>COVID-19 ინფექცია ბოლო 6 თვის განმავლობაში</Text>
      <Text style={styles.cardText}>არ დადასტურებია</Text>
    </View>

    </ScrollView>
  </View>
);
};

const styles = StyleSheet.create({
  dropDownIcon: {
    flex: 1
  },
  Card: {
    flex: 1,
    margin: 10,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 16,
    borderRadius: 6,
    shadowColor: '#ff0000', shadowOffset: { width: 10, height: 10 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 3,
    shadowRadius: 3,
  },
  cardText: {
    fontSize: 16,
    fontFamily: 'bpg-square-webfont',
    color: '#1c583c',
  }
});

export default Content;
